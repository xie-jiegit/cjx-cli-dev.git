const path = require('path');

module.exports = {
  entry: './core/cli/bin/core.js',
  output: {
    path: path.join(__dirname, '/core/cli/bin'),
    filename: 'core.min.js',
  },
  mode: 'development',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env'
            ],
            plugins: [
              [
                '@babel/plugin-transform-runtime',
                {
                  corejs: 3,
                  regenerator: true,
                  useESModules: true,
                  helpers: true,
                },
              ],
              [
                "@babel/plugin-transform-modules-commonjs"
              ]
            ],
          },
        },
      },
    ],
  },
};
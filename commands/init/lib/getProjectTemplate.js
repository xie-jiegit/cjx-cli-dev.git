const request = require('@cjx-cli-dev/request')

module.exports = function() {
  return request({
    url: '/project/template'
  })
}
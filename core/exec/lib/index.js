'use strict';

module.exports = exec;
const path =require('path')

const log = require('@cjx-cli-dev/log')
const { execSpawn } = require('@cjx-cli-dev/utils')
const Package = require('@cjx-cli-dev/package')


const SETTINGS = {
  init: '@cjx-cli-dev/init'
  // init: '@imooc-cli/init'
}

const CACHE_DIR = 'dependencies'

async function exec() {
  // console.log('test exec')

  // 1.targetPAth -> modulePath
  // 2.modulePath -> Package(npm模块)
  // 3.Package.getRootFile(获取入口文件)
  // 4. Package.update / Pack.install

  // 封装 -> 复用 Package
  // console.log('exec', arguments)
  let targetPath = process.env.CLI_TARGET_PATH
  let homePath = process.env.CLI_HOME_PATH
  log.verbose('targetPath', targetPath)
  log.verbose('homePath', homePath)
  let storeDir
  let pkg

  const cmdObj = arguments[arguments.length - 1]
  
  const cmdName = cmdObj.name()
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'// latest
  // console.log(1111, packageName)
  
  if (!targetPath) {
    targetPath = path.resolve(homePath, CACHE_DIR)// 生成缓存路径
    storeDir = path.resolve(targetPath, 'node_modules')
    log.verbose(targetPath)
    log.verbose(storeDir)
    pkg = new Package({
      targetPath,
      storeDir,
      packageName,
      packageVersion
    })
    if (await pkg.exists()) {
      // 更新package
      log.verbose('更新package')
      await pkg.update()
    } else {
      // 安装package
      await pkg.install()
    }
    
  } else {
    pkg = new Package({
      targetPath,
      storeDir,
      packageName,
      packageVersion
    })
  }
  // console.log(1111, await pkg.exists())
  
  const rootFile = pkg.getRootFilePath()
  // console.log('1111rootFile', rootFile)
  if (rootFile) {
    //在当前进程中调用
    // require(rootFile).call(null, Array.from(arguments))

    // 在node 子进程中调用
    // -e/执行代码 code/要执行的代码
    const args = Array.from(arguments)
    const cmd = args[args.length - 1]
    const o = Object.create(null)
    Object.keys(cmd).forEach(key => {
      if (cmd.hasOwnProperty(key) && !key.startsWith('_') && key !== 'parent') {
        o[key] = cmd[key]
      }
    })
    args[args.length - 1] = o
    // console.log(args)
    
    const code = `require('${rootFile}').call(null, ${JSON.stringify(args)})`
    
    execSpawn('node', ['-e', code], {
      cwd: process.cwd(),//当前命令执行
      stdio: 'inherit',// 通过相应的标准输入输出流传入/传出父进程。 在前三个位置，这分别相当于 process.stdin、process.stdout 和 process.stderr。 在任何其他位置，相当于 'ignore'。
    })

    // child.on('error', e => {
    //   // 执行失败
    //   log.error(e.message)
    //   process.exit(1)
    // })

    // child.on('exit', e => {
    //   // 执行失败
    //   log.verbose('命令执行成功：' + e)
    //   process.exit(e)
    // })

  }
  
}

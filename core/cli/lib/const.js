const LOWEST_NODE_VERSION = '12.0.0'
const DEFAULT_CLI_HOME = ".cjx-cli-dev"
const LOG_LEVEL = ''
const CJX_CLI_BASE_URL = 'http://www.cjxbys.top:9001/'

module.exports = {
  LOWEST_NODE_VERSION,
  DEFAULT_CLI_HOME,
  LOG_LEVEL,
  CJX_CLI_BASE_URL
}
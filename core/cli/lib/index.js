'use strict';

module.exports = core;

const path = require('path')

const semver = require('semver')// 比较版本
const colors = require('colors')
const userHome = require('user-home')
const pathEXists = require('path-exists').sync

// const utils = require('@cjx-cli-dev/utils')
const log = require('@cjx-cli-dev/log')
const exec = require('@cjx-cli-dev/exec')

const pkg = require('../package.json')
const constant = require('./const')

function core() {
  try {
    
    prepare()

    registerCommand()
  } catch(e) {
    log.error(e.message)

    if (process.env.LOG_LEVEL === 'verbose') {
      // 调试模式打印详细报错
      console.log(e)
    }
   
  }
}

async function prepare() {
    checkPkgVersion()
   
    checkRoot()
    checkUserHome()
    checkEnv()
    await checkGlobalUpdate()
}

// 检查版本号
function checkPkgVersion() {
  log.verbose('cli version', pkg.version)
}

// // 检查node 版本号
// function checkNodeVersion() {
//   // 获取当前版本号
//   const currentVersion = process.version
//   // 要求的最低版本号
//   const lowestVersion = constant.LOWEST_NODE_VERSION 
  
//   if (!semver.gte(currentVersion, lowestVersion)) {
//     throw new Error(colors.red(`cjx-cli 需要安装 v${lowestVersion} 以上版本的 Node.js`))
//   }
// }

// 检查是否是root 账户
// import rootCheck from 'root-check';
function checkRoot() {
  const rootCheck = require('root-check');
  rootCheck()
  // console.log(process.geteuid())
}

// 检查主目录
function checkUserHome() {
  if (!userHome || !pathEXists(userHome)) {
    throw new Error(colors.red('当前登录用户目录不存在'))
  }
}

// 检查环境变量 vim .env   esc > shit + : > wq!
let config
function checkEnv() {
  const dotenv = require('dotenv')
  const dotenvPAth = path.resolve(userHome, '.env')
  if (pathEXists(dotenvPAth)) {
    config = dotenv.config({
      path: dotenvPAth
    })
  }
  ceratedDefaultConfig()
  // log.verbose('环境变量', process.env.CLI_HOME_PATH)
}

function ceratedDefaultConfig() {
  const cliConfig = {
    home: userHome
  }
  if (process.env.CLI_HOME) {
    cliConfig['cliHome'] = path.join(userHome, process.env.CLI_HOME)
  } else {
    cliConfig['cliHome'] = path.join(userHome, constant.DEFAULT_CLI_HOME)
  }
  process.env.CLI_HOME_PATH = cliConfig.cliHome
  process.env.CJX_CLI_BASE_URL = constant.CJX_CLI_BASE_URL
}

// 检查是否最新版本 更新版本 lerna create get-npm-info ./utils/get-npm-info
async function checkGlobalUpdate() {
  // 1.获取当前版本号和模块名
  const currentVersion = pkg.version
  const npmName = pkg.name
  // 2.调用npm api, 获取所有版本号 // 3.提取所有版本号，比对那些版本号是大于当前版本号
  const { getNpmSemverVersions } = require('@cjx-cli-dev/get-npm-info')
  const lastVersions =  await getNpmSemverVersions(currentVersion, npmName)
  // console.log('versions: ', lastVersions)
  
  // 4.调用最新版本号，提示用户更新到该版本
  if (lastVersions && semver.gt(lastVersions, currentVersion)) {
    log.warn(colors.yellow(`请手动更新 ${npmName}，当前版本：${currentVersion}，最新版本：${lastVersions}
          更新命令：npm install -g ${npmName}`)
    )
  }
  
}

// npm i -S commander 注册命令
const { Command } = require('commander')
const program = new Command()
function registerCommand() {
  program
  .name(Object.keys(pkg.bin)[0])
  .usage('<command> [options]')
  .version(`${pkg.name} ${pkg.version}`)
  .option('-d, --debug', '是否开启调试模式', false)
  .option('-tp, --targetPath <targetPath>', '是否指定本地调试文件路径', '')

  const option = program.opts()

  // 注册命令 init命令 
  program
    .command('init [projectName]')
    .option('-f, --force', '是否强制初始化项目')
    .action(exec)
    // .action((projectName, options) => {
    //   console.log('init', projectName, options)
    // })
  
 
  // 指定 缓存package包的 targetPath/目标路径
  program.on('option:targetPath', function() {
    process.env.CLI_TARGET_PATH = option.targetPath
    // console.log(1111, program.opts().targetPath)
  })


  program.on('option:debug', function() {
    // console.log(option)
    if (option.debug) {
      process.env.LOG_LEVEL = 'verbose'
    } else {
      process.env.LOG_LEVEL = 'info'
    }
    log.level = process.env.LOG_LEVEL
    log.verbose(colors.red('debug 现在是调试模式'))
  })

  // 监听 未知命令
  program.on('command:*', function(obj) {
    const availableCommands = Object.keys(program.opts())
    console.log(colors.red('未知命令：' + obj[0]))
    console.log(colors.red('可用命令：' + availableCommands.join(',')))
  })

  // 后面没有输入命令 打开帮助文档
  if (process.argv.length < 3) {
    program.outputHelp()
  }

  program.parse(process.argv)
}
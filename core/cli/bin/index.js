#!/usr/bin/env node

const importLocal = require('import-local')
// require('./core.js')



// 检查是否本地版本
if (importLocal(__filename)) {
  // process.env.CLI_LOCAL = 'yewLocal'
  require('npmlog').info('cli', '正在使用 cjx-cli 本地版本')
} else {
  // process.env.CLI_LOCAL = 'noLocal'
  require('../lib')(process.argv.slice(2))
}


// 兼容es6 
// require('./core.min.js')


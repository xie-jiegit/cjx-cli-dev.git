function sum(a, b) {
  return a + b
} 


const pathExists = require('path-exists');
function exists(p) {
  return pathExists.sync(p)
}

module.exports = {
  exists,
  sum
}
